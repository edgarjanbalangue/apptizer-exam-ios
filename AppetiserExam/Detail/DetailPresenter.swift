//
//  DetailPresenter.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

class DetailPresenter {
    
    var view: DetailViewProtocol!
    var track: TrackModel?
    
    init(view: DetailViewProtocol, track: TrackModel) {
        self.view = view
        self.track = track
        show()
    }
    
    
    //return all details when its not null
    private func show() {
        if self.track != nil {
            view.setupView(track: track!)
        }
    }
    
    
}
