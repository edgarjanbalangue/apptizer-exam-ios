//
//  DetailViewController.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    @IBOutlet weak var artworkIv: UIImageView!
    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var priceBtn: UIButton!
    @IBOutlet weak var longDescriptionLbl: UILabel!
    
    var presenter: DetailPresenter!
    var track: TrackModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Add border on rating view and price button for better uI
        ratingView.layer.borderWidth = 0.5
        ratingView.layer.borderColor = UIColor.white.cgColor
        priceBtn.layer.borderWidth = 0.5
        priceBtn.layer.borderColor = UIColor.white.cgColor
        priceBtn.layer.cornerRadius = 3
        
        // Set presenter of the view
        presenter = DetailPresenter(view: self, track: self.track!)
    }
    
}

extension DetailViewController: DetailViewProtocol {

    
    func setupView(track: TrackModel) {
        if let urlString = track.artworkURL {
            let url = URL(string: urlString)
            artworkIv.kf.setImage(with: url)
        }
        
        trackNameLbl.text = track.name
        ratingLbl.text = track.rating
        genreLbl.text = track.genre
        priceBtn.setTitle("$" + (track.price ?? "0.00"), for: .normal)
        longDescriptionLbl.text = track.longDescription
    }
    
}


