//
//  DetailViewProtocol.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

protocol DetailViewProtocol: NSObjectProtocol {

    func setupView(track: TrackModel)
}

