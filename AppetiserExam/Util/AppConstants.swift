//
//  AppConstants.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/6/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

struct AppConstants {
    
    struct ViewItems {
        
        struct Search {
            static let name = "AppetiserExam.SearchViewController"
        }
        
        struct Detail {
            static let name = "AppetiserExam.DetailViewController"
        }
    }
    
    struct SegueName {
        static let showDetail = "showDetail"
    }
    
    struct TableVeiwCells {
        static let movieItem = "MovieItemTableViewCell"
    }
    
    struct ErrorMessages {
        static let failedToLoad = "Failed to load"
        static let errorOccur = "An error has occured."
    }
    
    struct ButtonNames {
        static let ok = "OK"
    }
    
    struct DateFormat {
        static let displayDateTime  = "MMMM dd,yyyy h:mm a"
    }
    
    struct AppLocalKey {
        static let lastDateVisited = "AppetiserExam.LastDateVisited"
        static let lastScreen = "AppetiserExam.LastScreen"
        static let trackId = "AppetiserExam.TrackId"
    }
    
    struct ApiURLS {
        static let baseUrl = "https://itunes.apple.com"
        static let getMovies = "/search?term=star&country=au&media=movie&all"
    }
    
    struct Model {
        struct Movie {
            
            static let id = "trackId"
            static let name = "trackName"
            static let price = "trackPrice"
            static let genre = "primaryGenreName"
            static let artwork = "artworkUrl100"
            static let longDescription = "longDescription"
            static let kind = "kind"
            static let rating = "contentAdvisoryRating"
            
            struct Kind {
                static let movie = "feature-movie" 
            }
        }
    }
}
