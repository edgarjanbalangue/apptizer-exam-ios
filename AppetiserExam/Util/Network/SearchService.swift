//
//  SearchService.swift
//  AppetiserExam
//
//  The Interactor class, contains: API call for specific endpoint to be called in The Presenter;
//      local data storing logic.
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

class SearchService {
    
    
    // MARK: Search Movie API
    // Get all the movie
    public func callAPIGetMovie(onSuccess successCallback: ((_ people: [TrackModel]) -> Void)?,
                                 onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        AppRemoteApi.instance.callAPIGetMovie(
            onSuccess: { (people) in
                successCallback?(people)
        },
            onFailure: { (errorMessage) in
                failureCallback?(errorMessage)
        }
        )
    }
}
