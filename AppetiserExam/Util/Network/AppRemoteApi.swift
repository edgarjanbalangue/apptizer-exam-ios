//
//  AppRemoteApi.swift
//  AppetiserExam
//
//  Populate all API caller methods.
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Foundation


class AppRemoteApi {
    static let instance = AppRemoteApi()
    
    enum RequestMethod {
        case get
        case post
    }
    
    // MARK: Search Movie API
    func callAPIGetMovie(onSuccess successCallback: ((_ people: [TrackModel]) -> Void)?,
                          onFailure failureCallback: ((_ errorMessage: String) -> Void)?) {
        
        // Build URL
        let url = AppConstants.ApiURLS.baseUrl + AppConstants.ApiURLS.getMovies
        
        // call API
        self.createRequest(
            url, method: .get, headers: nil, parameters: nil,
            onSuccess: {(responseObject: JSON) -> Void in
                // Create dictionary
                if let responseDict = responseObject["results"].arrayObject {
                    let trackDict = responseDict as! [[String:AnyObject]]
                    
                    // Create object
                    var data = [TrackModel]()
                    for item in trackDict {
                        let track = TrackModel.build(item)
                        if let kind = track.kind {
                            if kind == AppConstants.Model.Movie.Kind.movie {
                                data.append(track)
                            }
                        }
                    }
                    
                    // Fire callback
                    successCallback?(data)
                } else {
                    failureCallback?(AppConstants.ErrorMessages.errorOccur)
                }
        },
            onFailure: {(errorMessage: String) -> Void in
                failureCallback?(errorMessage)
        }
        )
    }
    
    // MARK: Request Handler
    // Create request
    private func createRequest(
        _ url: String,
        method: HTTPMethod,
        headers: [String: String]?,
        parameters: AnyObject?,
        onSuccess successCallback: ((JSON) -> Void)?,
        onFailure failureCallback: ((String) -> Void)?
        ) {
        
        AF.request(url, method: method).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                successCallback?(json)
            case .failure(let error):
                if let callback = failureCallback {
                    // Return
                    callback(error.localizedDescription)
                }
            }
        }
    }
}
