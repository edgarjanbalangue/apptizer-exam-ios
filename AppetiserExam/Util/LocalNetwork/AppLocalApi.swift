//
//  AppLocalApi.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

class AppLocalApi {
    
    static func saveUserLastVisitDateTime() {
        let df = DateFormatter()
        df.dateFormat = AppConstants.DateFormat.displayDateTime
        let now = df.string(from: Date())
        let defaults = UserDefaults.standard
        defaults.set(now, forKey: AppConstants.AppLocalKey.lastDateVisited)
        defaults.synchronize()
    }
    
    static func getUserLastVisitDateTime() -> String? {
        return UserDefaults.standard.string(forKey: AppConstants.AppLocalKey.lastDateVisited)
    }
    
    static func saveUserLastScreen(screen: String) {
        let defaults = UserDefaults.standard
        defaults.set(screen, forKey: AppConstants.AppLocalKey.lastScreen)
        defaults.synchronize()
    }
    
    static func getUserLastScreen() -> String? {
        return UserDefaults.standard.string(forKey: AppConstants.AppLocalKey.lastScreen)
    }
    
    static func saveTrackId(trackID: Int) {
        let defaults = UserDefaults.standard
        defaults.set(trackID, forKey: AppConstants.AppLocalKey.trackId)
        defaults.synchronize()
    }
    
    static func getTrackId() -> Int {
        return UserDefaults.standard.integer(forKey: AppConstants.AppLocalKey.trackId)
    }
}
