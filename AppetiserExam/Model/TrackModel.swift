//
//  TrackModel.swift
//  AppetiserExam
//  Model Class
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

class TrackModel {
    var id: Int?
    var name: String?
    var price: String?
    var genre: String?
    var artworkURL: String?
    var longDescription: String?
    var kind: String?
    var rating: String?
    
    // MARK: Instance Method
    func loadFromDictionary(_ dict: [String: AnyObject]) {
        if let data = dict[AppConstants.Model.Movie.id] as? Int {
            self.id = data
        }
        
        if let data = dict[AppConstants.Model.Movie.name] as? String {
            self.name = data
        }
        
        if let data = dict[AppConstants.Model.Movie.price] as? Double {
            self.price = String(data)
        }
        
        if let data = dict[AppConstants.Model.Movie.genre] as? String {
            self.genre = data
        }
        
        if let data = dict[AppConstants.Model.Movie.artwork] as? String {
            self.artworkURL = data
        }
        
        if let data = dict[AppConstants.Model.Movie.longDescription] as? String {
            self.longDescription = data
        }
        
        if let data = dict[AppConstants.Model.Movie.kind] as? String {
            self.kind = data
        }
        
        if let data = dict[AppConstants.Model.Movie.rating] as? String {
            self.rating = data
        }
    }
    
    // MARK: Class Method
    class func build(_ dict: [String: AnyObject]) -> TrackModel {
        let contact = TrackModel()
        contact.loadFromDictionary(dict)
        return contact
    }
}
