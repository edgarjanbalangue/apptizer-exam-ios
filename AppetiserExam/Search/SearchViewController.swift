//
//  SearchViewController.swift
//  AppetiserExam
//
//  The App Main ViewController (has only one presenter).
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import UIKit
import Kingfisher

class SearchViewController: UITableViewController {

    let presenter = SearchPresenter (searchService: SearchService())
    var detailViewController: DetailViewController? = nil
    var tracks = [TrackModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: AppConstants.TableVeiwCells.movieItem, bundle: nil), forCellReuseIdentifier: AppConstants.TableVeiwCells.movieItem)
        
        
        // attach view into presenter
        presenter.attachView(view: self)
        
        // fetch movies
        presenter.getMovies()
        
        self.refreshControl?.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func refresh(sender:AnyObject)
    {
        presenter.getMovies()
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AppConstants.SegueName.showDetail {
            if let track = sender as? TrackModel {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.track = track
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View Datasource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppConstants.TableVeiwCells.movieItem, for: indexPath) as! MovieItemTableViewCell
        cell.selectionStyle = .none
        
        let track = tracks[indexPath.row]
        cell.setupView(track: track)
        if let urlString = track.artworkURL {
            let url = URL(string: urlString)
            
            cell.artworkIv.kf.setImage(with: url, placeholder: UIImage(named: "place_holder"), options: nil, progressBlock: nil, completionHandler: { result in
                cell.setNeedsLayout()
            })
        }
        

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let lastVisited = AppLocalApi.getUserLastVisitDateTime() {
            return "Last Visited : " + lastVisited
        }
        return nil
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.selectRow(at: indexPath.row)
        
    }
}

extension SearchViewController: SearchViewProtocol {
    func track(at index: Int) -> TrackModel {
        let track = self.tracks[index]
        return track
    }
    
    func move(track: TrackModel) {
        performSegue(withIdentifier: AppConstants.SegueName.showDetail, sender: track)
    }
    
    
    func startLoading() {
        self.refreshControl?.beginRefreshing()
    }
    
    func finishLoading() {
        self.refreshControl?.endRefreshing()
    }
    
    func setTracks(tracks: [TrackModel]) {
        self.tracks = tracks
        self.tableView.reloadData()
    }
    
    func hasError(error: String) {
        let alert = UIAlertController(title: AppConstants.ErrorMessages.failedToLoad, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: AppConstants.ButtonNames.ok, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func track(id: Int) -> TrackModel? {
        let track = tracks.first{ $0.id == id }
        return track
    }
    
    func moveToLastScreen(segueIdentifier: String, track: TrackModel?) {
        if track != nil {
            performSegue(withIdentifier: segueIdentifier, sender: track)
        }
    }
    
}
