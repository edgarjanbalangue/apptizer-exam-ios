//
//  SearchViewProtocol.swift
//  AppetiserExam
//
//  View: The ViewController's contract.
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

protocol SearchViewProtocol: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setTracks(tracks: [TrackModel])
    func hasError(error: String)
    func track(at index: Int) -> TrackModel
    func move(track: TrackModel)
    func track(id: Int) -> TrackModel?
    func moveToLastScreen(segueIdentifier: String, track: TrackModel?)
}
