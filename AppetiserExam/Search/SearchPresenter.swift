//
//  SearchPresenter.swift
//  AppetiserExam
//
//  The ViewController's presenter, contains:
//      - Presenter: The ViewController's main logic (call API, populate data, show data, etc.)
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import Foundation

class SearchPresenter {
    
    private let searchService:SearchService!
    weak private var searchView : SearchViewProtocol?
    
    init(searchService: SearchService) {
        self.searchService = searchService
    }
    
    func attachView(view: SearchViewProtocol) {
        searchView = view
    }
    
    func detachView() {
        searchView = nil
    }
    
    // Fetch all movies from itunes API
    func getMovies() {
        self.searchView?.startLoading()
        searchService.callAPIGetMovie(onSuccess: { (tracks) in
            self.searchView?.setTracks(tracks: tracks)
            self.searchView?.finishLoading()
            self.populateLastScreen()
        }) { (error) in
            self.searchView?.hasError(error: error)
            self.searchView?.finishLoading()
        }
    }
    
    // Select row via indexpath
    func selectRow(at index:Int) {
        let track = self.searchView?.track(at: index)
        self.searchView?.move(track: track!)
    }
    
    // Populate last screen if there is any
    func populateLastScreen() {
        if let lastScreen = AppLocalApi.getUserLastScreen() {
            switch lastScreen {
                
            //If last view was DetailViewContoller perform segue and also get track via id. If track is nil it will not go to detail view
            case AppConstants.ViewItems.Detail.name:
                self.searchView?.moveToLastScreen(segueIdentifier: AppConstants.SegueName.showDetail, track: self.searchView?.track(id: AppLocalApi.getTrackId()))
                break
            default:
                break
            }
        }
    }
    
    
}
