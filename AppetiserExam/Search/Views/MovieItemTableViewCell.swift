//
//  MovieItemTableViewCell.swift
//  AppetiserExam
//
//  Created by Edgar Jan Balangue on 3/5/20.
//  Copyright © 2020 Edgar Jan Balangue. All rights reserved.
//

import UIKit

class MovieItemTableViewCell: UITableViewCell {

    @IBOutlet weak var artworkIv: UIImageView!
    @IBOutlet weak var trackNameLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupView(track: TrackModel) {
        trackNameLbl.text = track.name
        genreLbl.text = track.genre
        priceLbl.text = "$" + (track.price ?? "0.00")
    }

}

