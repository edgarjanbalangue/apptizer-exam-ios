# Appetiser Exam IOS

This is a simple master-detail app on which it will fetch movies in itunes API.

  - TableViewController on which it will show all the movies
  - DetialedViewController will show a detailed view of selected movie.

# Persistence

  - Since its only saving a small piece of data like (screen_name, id, date) I decided to user UserDefualts on local caching. 
  - When I need to store more like big data or movie object for example I will use an actual database like RealM

# Architecture

 - I used MVP Architecture because it was cleaner than MVC. 
 
# Used cocoapods:
 - [Alamofire](https://github.com/Alamofire/Alamofire)
 - [Kingfisher](https://github.com/onevcat/Kingfisher)
 - [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)

